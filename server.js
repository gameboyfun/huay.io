let port = process.env.port || 3000;

const express = require('express'),
      bodyParser = require('body-parser'),
      actuator = require('express-actuator'),
      constants = require('./config/constants.config'),
      app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(actuator());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

require('./app/routes')(app);

if(constants.test){
	port = 3000;
}

app.listen(port, function() {
    console.log('Listening on : http://localhost:' + port);
});

module.exports = app;