'use strict';

module.exports = {
    "register": {
        "user_name": "required",
        "user_password": "required|min_length[8]",
        "first_name": "required",
        "last_name": "required",
        "bank_id": "required",
        "bank_account": "required",
        "bank_name": "required",
        "user_line": "required"
    },
    "login": {
        "user_name": "required",
        "user_password": "required"
    }
}