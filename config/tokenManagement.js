'use strict';

const ExtractJwt = require("passport-jwt").ExtractJwt,
      JwtStrategy = require("passport-jwt").Strategy,
      SECRET = require('../server'),
      jwtOptions = {
        jwtFromRequest: ExtractJwt.fromHeader("authorization"),
        secretOrKey: SECRET
    },
      jwtAuth = new JwtStrategy(jwtOptions, (payload, done) => {
        console.log(payload)
        if (payload.sub === "username") done(null, true);
        else done(null, false);
    });

module.exports = jwtAuth;