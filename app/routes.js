'use strict';

module.exports = function(app) {
    app.use('/', require('./controller/customer_route'))
    app.use('/', require('./controller/shop_route'))
    app.use('/', require('./controller/wallet_route'))
    app.use('/', require('./controller/withdraw_route'))
    app.use('/', require('./controller/deposit_route'))
    app.use('/', require('./controller/transaction_route'))
    app.use('/', require('./controller/auth'))
    // route path here e.g. app.use('/', require('./controllers/index'))
}

// app.get('/', function (req, res) {
//     res.send('hello world')
// })