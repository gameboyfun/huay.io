const database = require('../../library/db');

class CustomerModel{
    constructor(){
        this.db = new database();
        this.knex = this.db.getConnect();
    }
    get(table,id = null) {
        return new Promise((resolve, reject) => {
            let user = this.knex(table);
            if(id){
                user = user.where(id);
            }
            user.then((res) => {
                resolve(res)
            }).catch((error) => {
                reject(error)
            })
        })
    }
    create(data,table){
        return new Promise((resolve, reject) => {
            let user = this.knex(table);
            user.insert(data);
            user.then((res) => {
                resolve(res)
            }).catch((error) => {
                reject(error)
            })
        })
    }
    update(data,table){
        return new Promise((resolve, reject) => {
            let user = this.knex(table);
            let key = `${table}_id`;
            let id = data[`${table}_id`];
            delete data[`${table}_id`];
            user.where(key, id).update(data);
            user.then((res) => {
                resolve(res)
            }).catch((error) => {
                reject(error)
            })
        })
    }
    delete(id,table){
        return new Promise((resolve, reject) => {
            let user = this.knex(table);
            user.where(id).del();
            user.then((res) => {
                resolve(res)
            }).catch((error) => {
                reject(error)
            })
        })
    }
}

module.exports = CustomerModel;