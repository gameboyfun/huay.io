'use strict';

const express = require('express'),
      doHttp = require('../../library/doHttp'),
      requestConfig = require('../../config/response.config'),
      TransactionModel = require('../model/customer'),
      transaction = new TransactionModel(),
      http = new doHttp(),
      router = express.Router(),
      passport = require("passport"),
      requireJWTAuth = passport.authenticate("jwt",{session:false}),
      Auth = require('../../library/auth');

passport.use(Auth.jwtAuth);

// get all transaction
router.get('/transaction',requireJWTAuth, async (req, res) => {
    let table = 'transaction';
    try{
        let data = await transaction.get(table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// get one transaction
router.get('/transaction/:transaction_id',requireJWTAuth, async (req, res) => {
    let table = 'transaction';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await transaction.get(table,input);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// create transaction 
router.post('/transaction',requireJWTAuth, async (req, res) => {
    let table = 'transaction';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await transaction.create(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// update transaction
router.patch('/transaction/:transaction_id',requireJWTAuth, async (req, res) => {
    let table = 'transaction';
    let input = http.getInput(req);
    console.log("input : "+JSON.stringify(input))
    try{
        let data = await transaction.update(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// delete transaction
router.delete('/transaction/:transaction_id',requireJWTAuth, async (req, res) => {
    let table = 'transaction'
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await transaction.delete(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

module.exports = router;