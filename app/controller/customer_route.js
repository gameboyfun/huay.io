'use strict';

const express = require('express'),
      doHttp = require('../../library/doHttp'),
      requestConfig = require('../../config/response.config'),
      CustomerModel = require('../model/customer'),
      customer = new CustomerModel(),
      http = new doHttp(),
      router = express.Router(),
      passport = require("passport"),
      requireJWTAuth = passport.authenticate("jwt",{session:false}),
      Auth = require('../../library/auth');

passport.use(Auth.jwtAuth)

// get all customer
router.get('/customer', requireJWTAuth, async (req, res) => {
    let table = 'user';
    try{
        let data = await customer.get(table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// get one customer
router.get('/customer/:user_id', requireJWTAuth, async (req, res) => {
    let table = 'user';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await customer.get(table,input);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// create customer 
router.post('/customer', requireJWTAuth, async (req, res) => {
    let table = 'user';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await customer.create(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// update customer
router.patch('/customer/:user_id', requireJWTAuth, async (req, res) => {
    let table = 'user';
    let input = http.getInput(req);
    console.log("input : "+JSON.stringify(input))
    try{
        let data = await customer.update(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// delete customer
router.delete('/customer/:user_id', requireJWTAuth, async (req, res) => {
    let table = 'user';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await customer.delete(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

module.exports = router;