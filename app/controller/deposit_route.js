'use strict';

const express = require('express'),
      doHttp = require('../../library/doHttp'),
      requestConfig = require('../../config/response.config'),
      DepositModel = require('../model/customer'),
      deposit = new DepositModel(),
      http = new doHttp(),
      router = express.Router(),
      passport = require("passport"),
      requireJWTAuth = passport.authenticate("jwt",{session:false}),
      Auth = require('../../library/auth');

passport.use(Auth.jwtAuth);

// get all deposit
router.get('/deposit', requireJWTAuth, async (req, res) => {
    let table = 'deposit';
    try{
        let data = await deposit.get(table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// get one deposit
router.get('/deposit/:deposit_id',requireJWTAuth, async (req, res) => {
    let table = 'deposit';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await deposit.get(table,input);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// create deposit 
router.post('/deposit',requireJWTAuth, async (req, res) => {
    let table = 'deposit';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await deposit.create(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// update deposit
router.patch('/deposit/:deposit_id',requireJWTAuth, async (req, res) => {
    let table = 'deposit';
    let input = http.getInput(req);
    console.log("input : "+JSON.stringify(input))
    try{
        let data = await deposit.update(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// delete deposit
router.delete('/deposit/:deposit_id',requireJWTAuth, async (req, res) => {
    let table = 'deposit'
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await deposit.delete(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

module.exports = router;