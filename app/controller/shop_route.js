'use strict';

const express = require('express'),
      doHttp = require('../../library/doHttp'),
      requestConfig = require('../../config/response.config'),
      ShopModel = require('../model/customer'),
      shop = new ShopModel(),
      http = new doHttp(),
      router = express.Router(),
      passport = require("passport"),
      requireJWTAuth = passport.authenticate("jwt",{session:false}),
      Auth = require('../../library/auth');

passport.use(Auth.jwtAuth);

// get all shop
router.get('/shop',requireJWTAuth, async (req, res) => {
    let table = 'shop';
    try{
        let data = await shop.get(table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// get one shop
router.get('/shop/:shop_id',requireJWTAuth, async (req, res) => {
    let table = 'shop';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await shop.get(table,input);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// create shop 
router.post('/shop',requireJWTAuth, async (req, res) => {
    let table = 'shop';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await shop.create(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// update shop
router.patch('/shop/:shop_id',requireJWTAuth, async (req, res) => {
    let table = 'shop';
    let input = http.getInput(req);
    console.log("input : "+JSON.stringify(input))
    try{
        let data = await shop.update(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// delete shop
router.delete('/shop/:shop_id',requireJWTAuth, async (req, res) => {
    let table = 'shop'
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await shop.delete(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

module.exports = router;