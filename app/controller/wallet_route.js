'use strict';

const express = require('express'),
      doHttp = require('../../library/doHttp'),
      requestConfig = require('../../config/response.config'),
      WalletModel = require('../model/customer'),
      wallet = new WalletModel(),
      http = new doHttp(),
      router = express.Router(),
      passport = require("passport"),
      requireJWTAuth = passport.authenticate("jwt",{session:false}),
      Auth = require('../../library/auth');

passport.use(Auth.jwtAuth);

// get all wallet
router.get('/wallet',requireJWTAuth, async (req, res) => {
    let table = 'wallet';
    try{
        let data = await wallet.get(table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// get one wallet
router.get('/wallet/:wallet_id',requireJWTAuth, async (req, res) => {
    let table = 'wallet';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await wallet.get(table,input);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// create wallet 
router.post('/wallet',requireJWTAuth, async (req, res) => {
    let table = 'wallet';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await wallet.create(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// update wallet
router.patch('/wallet/:wallet_id',requireJWTAuth, async (req, res) => {
    let table = 'wallet';
    let input = http.getInput(req);
    console.log("input : "+JSON.stringify(input))
    try{
        let data = await wallet.update(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// delete wallet
router.delete('/wallet/:wallet_id',requireJWTAuth, async (req, res) => {
    let table = 'wallet'
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await wallet.delete(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

module.exports = router;