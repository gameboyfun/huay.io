'use strict';

const express = require('express'),
      doHttp = require('../../library/doHttp'),
      requestConfig = require('../../config/response.config'),
      WithdrawModel = require('../model/customer'),
      withdraw = new WithdrawModel(),
      http = new doHttp(),
      router = express.Router(),
      passport = require("passport"),
      requireJWTAuth = passport.authenticate("jwt",{session:false}),
      Auth = require('../../library/auth');

passport.use(Auth.jwtAuth);

// get all withdraw
router.get('/withdraw',requireJWTAuth, async (req, res) => {
    let table = 'withdraw';
    try{
        let data = await withdraw.get(table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// get one withdraw
router.get('/withdraw/:withdraw_id',requireJWTAuth, async (req, res) => {
    let table = 'withdraw';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await withdraw.get(table,input);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// create withdraw 
router.post('/withdraw',requireJWTAuth, async (req, res) => {
    let table = 'withdraw';
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await withdraw.create(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// update withdraw
router.patch('/withdraw/:withdraw_id',requireJWTAuth, async (req, res) => {
    let table = 'withdraw';
    let input = http.getInput(req);
    console.log("input : "+JSON.stringify(input))
    try{
        let data = await withdraw.update(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

// delete withdraw
router.delete('/withdraw/:withdraw_id',requireJWTAuth, async (req, res) => {
    let table = 'withdraw'
    let input = http.getInput(req);
    console.log(JSON.stringify(input))
    try{
        let data = await withdraw.delete(input,table);
        http.sendResponse(res, "200_success", data);
    }catch(err){
        console.log("error: " + err)
        http.sendResponse(res, "400_db_error", {error: err});
    }
})

module.exports = router;