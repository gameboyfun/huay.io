const express = require("express"),
      validate = require("simple-json-validate"),
      jwt = require("jwt-simple"),
      passport = require("passport"),
    
      doHttp = require('../../library/doHttp'),
      Auth = require('../../library/auth'),
      Database = require("../../library/db"),
      SECRET = require("../../config/constants.config").secretKey,
      paramConfig = require("../../config/params.config"),

      validated = new validate(paramConfig),
      http = new doHttp(),
      DB = new Database(),
      knex = DB.getConnect(),
      router = express.Router()

requireJWTAuth = passport.authenticate("jwt",{session:true});

router.post("/register", async (req, res) => {
    let input = http.getInput(req)
    let validData = validated.check("register", input)
    let code = "201_success";
    let output = {};
    try{
        console.log(JSON.stringify(validData))
        if(validData.error){
            code = "400_error_require";
            output = validData
        }else{
            let user = await knex('user').where("user_name", validData.user_name)
            if(user.length == 0){
                delete validData['isValid']
                console.log(JSON.stringify(validData))
                let insertResult = await knex('user').insert(validData)
                output = validData
                const payload = {
                    sub: req.body.username,
                    iat: new Date().getTime()
                 };
                 output.accessToken = jwt.encode(payload, SECRET);
            }else{
                code = "400_duplicate_data"
                output = {
                    message: `Error! usermame ${validData.username} already exist`
                }
            }  
        }
        console.log("output " +  JSON.stringify(output))
        http.sendResponse(res, code, output);
    } catch(e) {
        http.sendResponse(res, "500_Internal_Error", e.toString());
    }
    
})
router.post("/auth", Auth.loginMiddleWare(), async (req, res) => {
    const payload = {
       sub: req.body.user_name,
       iat: new Date().getTime()
    };
    let userdata = await knex('user').where("user_name", req.body.user_name)
    userdata[0].access_token = jwt.encode(payload, SECRET);
    http.sendResponse(res, "200_success", userdata);
})
// router.get("/auth/facebook", passport.authenticate('facebook'))
// router.get('/auth/facebook/callback',
//     passport.authenticate('facebook',{ 
//         successRedirect: '/profile',
//         failureRedirect: '/' 
//     })
// )
// router.get('/profile', (req, res) => {
//   res.json(req.user)
// })

// router.get("/user", requireJWTAuth, (req, res) => {
//     http.sendResponse(res, "200_success", "Hello World");
// })
module.exports = router;